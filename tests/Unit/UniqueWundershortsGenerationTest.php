<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Helpers\Helper;

class UniqueWundershortsGenerationTest extends TestCase
{
    /**
     * Assuring that the wundershort generation function generates unique shorts.
     * Assumption: the encoding is done on the auto increment ID using a dictionary.
     * So we will assume the encoding valid if a random 1 million range shorts are unique.
     *
     * @return void
     */
    public function test_example()
    {
        $uniqueShorts = true;
        $existingWundershorts = [];

        $helper = new Helper();
        $st = random_int(1, 9999999);
        for ( $i = 1; $i <= 1000000; $i++ ) {
          $wundershort = $helper->getWundershort( $st );
          if ( isset( $existingWundershorts[$wundershort] ) ) {
            $uniqueShorts = false;
            break;
          }
          $existingWundershorts[$wundershort] = true;
          $st++;
        }
        $this->assertTrue(true);
    }
}
