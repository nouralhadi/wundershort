<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Helpers\Helper;

class ValidWundershortDecodingTest extends TestCase
{
    /**
     * Assuring that wundershort decoding (retrieving original id) is working.
     *
     * @return void
     */
    public function test_example()
    {
        
        $helper = new Helper();
        $id = random_int( 999, 999999 );
        $wundershort = $helper->getWundershort( $id );
        $this->assertTrue( $id === $helper->decodeWundershort( $wundershort ) );
    }
}
