<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Helpers\Helper;

class ValidWundershortGenerationTest extends TestCase
{


    /**
     * Assuring that the encoding (short-url) works.
     *
     * @return void
     */
    public function test_example()
    {
        $valid_wundershort = true;
        
        $helper = new Helper();
        $id = random_int( 999, 999999 );
        $wundershort = $helper->getWundershort( $id );
        
        for ( $i = 0; $i < strlen( $wundershort ); $i++ ) {
          $char = $wundershort[$i];
          if ( $char >= 'a' && $char <= 'z' ) continue;
          if ( $char >= 'A' && $char <= 'Z' ) continue;
          if ( $char >= '0' && $char <= '9' ) continue;
          $valid_wundershort = false;
          break;
        }

        $this->assertTrue( strlen( $wundershort ) < 6 && $valid_wundershort );
    }
}
