<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Wundershort;
use Illuminate\Http\Request;
use Helper;
use Location;
use DB;

class WundershortController extends Controller
{

    /**
     * Show the form for creating a new Wundershort.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('wundershorts.new-wundershort');
    }

    /**
     * Store a newly created Wundershort.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate( [
        'title' => ['required', 'string', 'max:255'],
        'original_url' => ['required', 'url', 'max:255'],
      ]);

      $helper = new Helper();

      $wundershort = Wundershort::create([
        'title' => $request->title,
        'original_url' => $request->original_url,
        'user_id' => Auth::user()->id,
        'short_url' => ''
      ]);

      $wundershort->short_url = $helper->getWundershort( $wundershort->id );
      $wundershort->save();
      return redirect()->route('show-wundershort', $wundershort->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wundershort  $wundershort
     * @return \Illuminate\Http\Response
     */
    public function show(Wundershort $wundershort)
    {
      return view('wundershorts.show-wundershort', [ 'wundershort' => $wundershort ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wundershort  $wundershort
     * @return \Illuminate\Http\Response
     */
    public function edit(Wundershort $wundershort)
    {
      return view('wundershorts.edit-wundershort', [ 'wundershort' => $wundershort ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wundershort  $wundershort
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wundershort $wundershort)
    {
      $request->validate( [
        'title' => ['required', 'string', 'max:255'],
        'original_url' => ['required', 'url', 'max:255'],
      ]);
      
      $wundershort->title = $request->title;
      $wundershort->original_url = $request->original_url;
      $wundershort->save();
      return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wundershort  $wundershort
     * @return \Illuminate\Http\Response
     */
    public function delete(Wundershort $wundershort)
    {
        $wundershort->delete();
        return redirect()->route('home');
    }


    public function statistics() {
      return view( 'wundershorts.statistics', [ 'wundershorts' => Auth::user()->wundershorts ] );
    }

    public function oneStatistics( Wundershort $wundershort ) {
      $dailyVisitsStatistics = DB::table('wundershort_visits')
                 ->select(
                    DB::raw('DATE(created_at) as visitDate'),
                    DB::raw('count(*) as visits'),
                    DB::raw('count( distinct ip ) as uniqueIPs'),
                    DB::raw('count( distinct country ) as uniqueCountries'),
                    DB::raw('count( distinct region ) as uniqueRegions'),
                    )->where('wundershort_id', $wundershort->id)
                 ->groupBy('visitDate')->get();
      $wundershort->dailyVisitsStatistics = $dailyVisitsStatistics;

      $visitsStatistics = DB::table('wundershort_visits')->select('*')
                              ->where('wundershort_id', $wundershort->id)->get();
      
      $wundershort->visitsStatistics = $visitsStatistics;

      $chartData = [];
      if ( count( $visitsStatistics ) ) {
        foreach ( $visitsStatistics as $entry ) {
          if ( !isset( $chartData[$entry->country] ) ) {
            $chartData[$entry->country] = 1;
          } else {
            $chartData[$entry->country] += 1;
          }
        }
      }
      $wundershort->chartData = $chartData;
      return view( 'wundershorts.oneStatistics', [ 'wundershort' => $wundershort ] );
    }
}
