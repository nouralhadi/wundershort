<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use DB;
use Helper;
use Location;
use App\Models\Wundershort;

class EnsureUniqueVisits
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ( !$request->wundershort ) return redirect()->route('home');
        $wundershortId = ( new Helper() )->decodeWundershort($request->wundershort);
        if ( !Wundershort::find($wundershortId) ) return redirect()->route('home');

        $lastVisits = DB::table('wundershort_visits')->where( 'ip', $request->ip() )->whereRaw('created_at >= now() - interval 5 minute')->get();
        if ( $lastVisits->isEmpty() ) {
          $location = Location::get( $request->ip() );
          DB::table('wundershort_visits')->insert([
            'wundershort_id' => $wundershortId,
            'ip' => $request->ip(),
            'country' => ( $location ? $location->countryName : NULL ),
            'region' => ( $location ? $location->regionName : NULL )
          ]);
        }
        return $next($request);
    }
}
