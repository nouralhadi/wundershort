<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wundershort extends Model {
    use HasFactory;

    // Fillable array required for the create static method of the model
    protected $fillable = [ 'title', 'user_id', 'original_url', 'short_url' ];

    public function user() {
      return $this->belongsTo(User::class);
    }
}
