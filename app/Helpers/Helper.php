<?php

namespace App\Helpers;

class Helper {


  private $dictionary = [ 
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
  ];

  public function __construct() { 
  }

  public function getWundershort( $id ) {
    $wundershort = '';
    
    $dictLength = count($this->dictionary);
    while ($id > 0) {
      $nextCharIndex = $id % $dictLength;
      $wundershort .= $this->dictionary[$nextCharIndex];
      $id = intdiv( $id, $dictLength );
    }

    return $wundershort;
  }

  public function decodeWundershort( $wundershort ) {
    $id = 0;
    $pow = 1;
    $dictLength = count( $this->dictionary );

    for( $i = 0; $i < strlen($wundershort); $i++ ) {
      $nextChar = $wundershort[$i];
      $nextIdx = intval( array_search( $nextChar, $this->dictionary ) );
      if ( $nextIdx === false || $nextIdx === 0 ) return 0;
      $id += ( $nextIdx * $pow );
      $pow *= $dictLength;
    }

    return $id;
  }

  
}