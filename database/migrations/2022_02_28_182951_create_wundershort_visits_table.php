<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wundershort_visits', function (Blueprint $table) {
            $table->id();
            $table->foreignId('wundershort_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->string('ip');
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wundershort_visits');
    }
};
