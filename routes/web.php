<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WundershortController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


require __DIR__.'/auth.php';

Route::get('/', function () {
  return view('home');
})->name('home');

Route::middleware(['auth', 'verified'])->group(function () {
  Route::get('/wundershort/create', [WundershortController::class, 'create'])->name('new-wundershort');
  Route::post('/wundershort/store', [WundershortController::class, 'store'])->name('store-wundershort');
  Route::get('/wundershort/edit/{wundershort}', [WundershortController::class, 'edit'])->name('edit-wundershort');
  Route::post('/wundershort/update/{wundershort}', [WundershortController::class, 'update'])->name('update-wundershort');
  Route::post('/wundershort/delete/{wundershort}', [WundershortController::class, 'delete'])->name('delete-wundershort');
  Route::get('/wundershort/{wundershort}', [WundershortController::class, 'show'])->name('show-wundershort');
  Route::get('/statistics', [WundershortController::class, 'statistics'])->name('statistics');
  Route::get('/statistics/{wundershort}', [WundershortController::class, 'oneStatistics'])->name('one-statistics');
});

Route::middleware(['uniqueVisits'])->group(function () {
  Route::get('/{wundershort}', function ( $wundershort ) {
    $helper = new \App\Helpers\Helper();
    $wundershortId = $helper->decodeWundershort( $wundershort );
    $wundershortObject = \App\Models\Wundershort::find( $wundershortId );
    return view('wunderout', [ 'finalDestination' => $wundershortObject->original_url ] );
  })->name('wundershort');
});