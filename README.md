<h1 align="center" style="color: green">Wundershort</h1>


**Demo Available Here: [Wundershort](https://wundershort.herokuapp.com/)**

## About Wundershort

Yet another URL shortener, technical specifications:

- Built with Laravel 9 / PHP 8.
- Powered by MySQL.
- Testing is done using PHPUnit.
- Code is maintained on Bitbucket (as you can see :D ).
- App is hosted on Heroku.
- Thanks to Bitbucket pipelines, this repository is automatically deployed to heroku.


## Features

- Brand new fast & accurate shortening algorithm.
- Accurate visits logs depending on the clients ip.
- Keeping you informed about the geographical locations of your Wundershorts users.
- Good looking visual representation of data.

## Upcoming updates

- Visuals & UI/UX fixes.
- Profile management.
- Authorization & Admin mode.

## Hey there

Made with :blue_heart: by Nour Alhadi Mahmoud.