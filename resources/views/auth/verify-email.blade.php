@extends('layouts.main')
@section('content')
  <div class="container">
    <div class="row">
      <div class="card offset-2 col-8 mt-4 px-0">
        <div class="card-header">Forgot your password!</div>
        <div class="card-body pb-4">
          <div class="row">

            <div class="alert alert-info mb-4 text-sm text-gray-600">
                Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn't receive the email, we will gladly send you another.
            </div>

            @if (session('status') == 'verification-link-sent')
                <div class="alert alert-info mb-4 font-medium text-sm text-green-600">
                    A new verification link has been sent to the email address you provided during registration.
                </div>
            @endif

            <div class="mt-4 flex items-center justify-between">
                <form class="d-inline-flex" method="POST" action="{{ route('verification.send') }}">
                    @csrf                  
                    <button class="ml-3 btn btn-success">Resend Verification Email</button>
                </form>

                <form class="d-inline-flex" method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button class="ml-3 btn btn-success" >Logout</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

