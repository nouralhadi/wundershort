@extends('layouts.main')
@section('content')
  <div class="container">
    <div class="row">
      <div class="card offset-2 col-8 mt-4 px-0">
        <div class="card-header">Reset password!</div>
        <div class="card-body pb-4">
          <div class="row">

            @if( count( $errors ) )
              <div class="alert alert-danger offset-3 col-6">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
              </div>
              <div class="offset-3"></div>
            @endif


            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <!-- Password Reset Token -->
                <input type="hidden" name="token" value="{{ $request->route('token') }}">

                <!-- Email Address -->
                <div class="form-group mt-4">
                  <label class="form-label" for="email">Email: </label>
                  <input id="email" class="form-control block mt-1" type="email" name="email" value="{{ old('email', $request->email) }}" required autofocus />
                </div>

                <!-- Password -->
                <div class="form-group mt-4">
                  <label class="form-label" for="password">Password: </label>
                  <input id="password" class="form-control block mt-1" type="password" name="password" required/>
                </div>

                <!-- Confirm Password -->
                <div class="form-group mt-4">
                  <label class="form-label" for="password_confirmation">Confirm Password: </label>
                  <input id="password_confirmation" class="form-control block mt-1" type="password" name="password_confirmation" required/>
                </div>

                <div class="flex items-center justify-end mt-4">
                  <button class="ml-3 btn btn-success" >Reset password</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
