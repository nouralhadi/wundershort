@extends('layouts.main')
@section('content')
  <div class="container">
    <div class="row">
      <div class="card offset-2 col-8 mt-4 px-0">
        <div class="card-header">Login to Wundershort!</div>
        <div class="card-body pb-4">
          <div class="row">
            @if( count( $errors ) )
              <div class="alert alert-danger offset-3 col-6">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
              </div>
              <div class="offset-3"></div>
            @endif

            <form class="offset-3 col-6" method="POST" action="{{ route('login') }}">
                @csrf

                <!-- Email Address -->
                <div class="form-group mt-4">
                    <label class="form-label" for="email">Email: </label>
                    <input id="email" class="form-control block mt-1" type="email" name="email" value="{{ old('email') }}" required autofocus />
                </div>

                <!-- Password -->
                <div class="form-group mt-4">
                    <label class="form-label" for="password">Password: </label>
                    <input id="password" class="form-control block mt-1" type="password" name="password" required autocomplete="current-password" />
                </div>

                <!-- Remember Me -->
                <div class="block mt-4">
                    <label for="remember_me" class="inline-flex items-center">
                        <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                        <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                    </label>
                    @if (Route::has('password.request'))
                        <a class="float-end nostyle text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                            {{ __('Forgot your password?') }}
                        </a>
                    @endif
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button class="ml-3 btn btn-success" >Login</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
