@extends('layouts.main')
@section('content')
  <div class="container">
    <div class="row">
      <div class="card offset-2 col-8 mt-4 px-0">
        <div class="card-header">Confirm password!</div>
        <div class="card-body pb-4">
          <div class="row">

            <div class="alert alert-info mb-4 text-sm text-gray-600">
                This is a secure area of the application. Please confirm your password before continuing.
            </div>

            @if( count( $errors ) )
              <div class="alert alert-danger offset-3 col-6">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
              </div>
              <div class="offset-3"></div>
            @endif

            <form class="offset-3 col-6" method="POST" action="{{ route('password.confirm') }}">
                @csrf

                <!-- Password -->
                <div class="form-group mt-4">
                  <label class="form-label" for="password">Password: </label>
                  <input id="password" class="form-control block mt-1" type="password" name="password" required autocomplete="current-password" />
                </div>

                <div class="flex items-center justify-end mt-4">
                  <button class="ml-3 btn btn-success" >Confirm Password</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
