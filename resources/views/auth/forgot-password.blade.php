@extends('layouts.main')
@section('content')
  <div class="container">
    <div class="row">
      <div class="card offset-2 col-8 mt-4 px-0">
        <div class="card-header">Forgot your password!</div>
        <div class="card-body pb-4">
          <div class="row">

            <div class="mb-4 text-sm alert alert-info text-gray-600">
                Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.
            </div>


            @if( count( $errors ) )
              <div class="alert alert-danger offset-3 col-6">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
              </div>
              <div class="offset-3"></div>
            @endif

            <form class="offset-3 col-6" method="POST" action="{{ route('password.email') }}">
                @csrf

                <!-- Email Address -->
                <div class="form-group mt-4">
                  <label class="form-label" for="email">Email: </label>
                  <input id="email" class="form-control block mt-1" type="email" name="email" value="{{ old('email') }}" required autofocus />
                </div>

                <div class="flex items-center justify-end mt-4">
                  <button class="ml-3 btn btn-success" >Email Password Reset Link</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

