@extends('layouts.main')
@section('content')
  <div class="container">
    <div class="row">
      <div class="card offset-2 col-8 mt-4 px-0">
        <div class="card-header">Register to Wundershort!</div>
        <div class="card-body pb-4">
          <div class="row">
            @if( count( $errors ) )
              <div class="alert alert-danger offset-3 col-6">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
              </div>
              <div class="offset-3"></div>
            @endif

            <form class="offset-3 col-6" method="POST" action="{{ route('register') }}">
              @csrf

              <!-- Name -->
              <div class="form-group mt-4">
                <label class="form-label" for="name">Name: </label>
                <input id="name" class="form-control block mt-1" type="text" name="name" value="{{ old('name') }}" required autofocus />
              </div>

              <!-- Email Address -->
              <div class="form-group mt-4">
                <label class="form-label" for="email">Email: </label>
                <input id="email" class="form-control block mt-1" type="email" name="email" value="{{ old('email') }}" required autofocus />
              </div>

              <!-- Password -->
              <div class="form-group mt-4">
                <label class="form-label" for="password">Password: </label>
                <input id="password" class="form-control block mt-1" type="password" name="password" required autocomplete="new-password" />
              </div>

              <!-- Confirm Password -->
              <div class="form-group mt-4">
                <label class="form-label" for="password_confirmation">Confirm Password: </label>
                <input id="password_confirmation" class="form-control block mt-1" type="password" name="password_confirmation" required/>
              </div>

              <div class="flex items-center justify-end mt-4 mb-4">
                  <button class="ml-3 btn btn-success" >Register</button>
                  <a class="nostyle float-end text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    Already registered?
                  </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

