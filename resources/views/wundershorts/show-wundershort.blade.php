@extends('layouts.main')
@section('content')
  <div class="row">
    <div class="col-12">
      <div class="row">
        <div class="offset-md-4 col-md-6 col-sm-12 d-flex align-items-stretch">
          <div class="card text-center">
            <div class="card-title">
              <img style="width: 200px; height: 200px" src="{{ asset('images/mario-walking.gif') }}" class="card-img-top" alt="mario-walking">
            </div>
            <div class="card-body">
              <h5 class="card-title">Thanks for using Wundershort&trade;</h5>
              <p class="card-text">You can access this wundershort's statistics page by clicking <a class="nostyle text-success fw-bold" href="{{ route( 'one-statistics', $wundershort->id ) }}">here</a> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
