@extends('layouts.main')
@section('content')
  <div class="row">
    <div class="offset-2 col-8">
      <div class="card">
        <div class="card-body">
          <div class="row text-center">
            <div class="card-title">My Statistics</div>
          </div>
          <div class="row">
            <div class="col-12">
              <div id="wrapper"></div>
              <table id="statistics" class="table table-responsive d-none">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Original URL</th>
                    <th>Short URL</th>
                    <th>Created At</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($wundershorts as $wundershort)
                  <tr>
                    <td><a href="{{ route( 'one-statistics', $wundershort->id ) }}" class="nostyle text-success fw-bold">{{ $wundershort->title }}</a></td>
                    <td><a href="{{ $wundershort->original_url }}" target="_blank" class="nostyle text-success fw-bold">{{ $wundershort->original_url }}</a></td>
                    <td><a href="{{ route( 'wundershort', $wundershort->short_url ) }}" target="_blank" class="nostyle text-success fw-bold">{{  $wundershort->short_url }}</a></td>
                    <td>{{ substr( $wundershort->created_at, 0, -3 ) }}</td>
                    <td> 
                      <a class="nostyle btn btn-sm btn-warning my-2" href="{{ route('edit-wundershort', $wundershort->id) }}">Update</a>
                      <form method="POST" action="{{ route('delete-wundershort', $wundershort->id) }}">
                        @csrf
                        <button type="button" class="btn btn-sm btn-danger" onclick="confirmDelete(this)">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>

  
@endsection

@section('scripts')
<script>
  const grid = new gridjs.Grid({ 
    from: document.getElementById('statistics'),
    pagination: { limit: 10 },
    search: true,
    sort: true,
  }).render(document.getElementById('wrapper'));
  grid.config.header._columns[4].sort.enabled = false;

  const confirmDelete = (element) => {
    Swal.fire({
      title: 'Are you sure?',
      text: "Once you delete a wundershort, we will automatically delete everything related to it, and you can never get it back.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#226F54',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        element.closest('form').submit();
      }
    });
  }
  
</script>
@endsection