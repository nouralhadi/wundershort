@extends('layouts.main')
@section('content')
  <div class="row">
    <div class="offset-2 col-8">
      <div class="card">
        <div class="card-body">
          <div class="row text-center">
            <div class="card-title">Wundershort: {{ $wundershort->title  }} - Statistics</div>
          </div>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link nostyle text-success fw-bold active" id="detailed-tab" data-bs-toggle="tab" data-bs-target="#detailed" type="button" role="tab" aria-controls="detailed" aria-selected="true">Detailed</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link nostyle text-success fw-bold" id="daily-tab" data-bs-toggle="tab" data-bs-target="#daily" type="button" role="tab" aria-controls="daily" aria-selected="false">Daily</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link nostyle text-success fw-bold" id="visual-tab" data-bs-toggle="tab" data-bs-target="#visual" type="button" role="tab" aria-controls="visual" aria-selected="false">Visual</button>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="detailed" role="tabpanel" aria-labelledby="detailed-tab">
              <div class="row">
                <div class="col-12">
                  <div id="detailed-wrapper"></div>
                  <table id="detailed-statistics" class="table table-responsive d-none">
                    <thead>
                      <tr>
                        <th>IP</th>
                        <th>Country</th>
                        <th>Region</th>
                        <th>Visit Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($wundershort->visitsStatistics as $statistic)
                      <tr>
                        <td>{{ $statistic->ip }}</td>
                        <td>{{ $statistic->country }}</td>
                        <td>{{ $statistic->region }}</td>
                        <td>{{ substr( $statistic->created_at, 0, -3 ) }}</td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="daily" role="tabpanel" aria-labelledby="daily-tab">
              <div class="row">
                <div class="col-12">
                  <div id="daily-wrapper"></div>
                  <table id="daily-statistics" class="table table-responsive d-none">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>Number of visits</th>
                        <th>Unique IPs</th>
                        <th>Unique Countries</th>
                        <th>Unique Regions</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($wundershort->dailyVisitsStatistics as $statistic)
                      <tr>
                        <td>{{ $statistic->visitDate }}</td>
                        <td>{{ $statistic->visits }}</td>
                        <td>{{ $statistic->uniqueIPs }}</td>
                        <td>{{ $statistic->uniqueCountries }}</td>
                        <td>{{ $statistic->uniqueRegions }}</td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="visual" role="tabpanel" aria-labelledby="visual-tab">
              <div class="row">
                <div class="col-12">
                  <div id="visitorsByCountry" class="mx-5 my-5" style="width: 100%; height: 400px;"></div>
                  <div id="noChartData" class="alert alert-warning d-none">Your Wundershort does not have any visitors yet :(, please check again!</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>

  
@endsection

@section('scripts')
<script>
  new gridjs.Grid({ 
    from: document.getElementById('detailed-statistics'),
    pagination: { limit: 10 },
    search: true,
    sort: true,
  }).render(document.getElementById('detailed-wrapper'));

  new gridjs.Grid({ 
    from: document.getElementById('daily-statistics'),
    pagination: { limit: 10 },
    search: true,
    sort: true,
  }).render(document.getElementById('daily-wrapper'));

  
  const defineChart = ( chartId, chartTitle, chartData ) => {
    if ( chartData.length === 0 ) {
      document.getElementById("noChartData").classList.remove("d-none");
      document.getElementById(chartId).classList.add('d-none');
      return;
    }
    am5.ready(function() {

      const root = am5.Root.new( chartId );
      root.setThemes([
        am5themes_Animated.new(root)
      ]);


      const chart = root.container.children.push(am5percent.PieChart.new(root, {
        layout: root.verticalLayout,
        innerRadius: am5.percent(50)
      }));


      const series = chart.series.push(am5percent.PieSeries.new(root, {
        valueField: "value",
        categoryField: "country",
        alignLabels: false
      }));

      series.labels.template.setAll({
        textType: "circular",
        centerX: 0,
        centerY: 0
      });


      series.data.setAll( chartData );


      const legend = chart.children.push(am5.Legend.new(root, {
        centerX: am5.percent(50),
        x: am5.percent(50),
        marginTop: 15,
        marginBottom: 15,
      }));

      legend.data.setAll(series.dataItems);
      series.appear(1000, 100);

      chart.children.unshift(am5.Label.new(root, {
        text: chartTitle,
        fontSize: 25,
        fontWeight: "500",
        textAlign: "center",
        x: am5.percent(50),
        centerX: am5.percent(50),
        paddingTop: 0,
        paddingBottom: 0
      }));

    });
  }

  defineChart( "visitorsByCountry", "Visitors by country", [
        @foreach( $wundershort->chartData as $key => $value )
          { category: '{{ $key }}', value: {{ $value }} },
        @endforeach
      ] );

</script>
@endsection