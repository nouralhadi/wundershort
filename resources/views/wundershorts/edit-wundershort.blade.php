@extends('layouts.main')
@section('content')
  <div class="row">
    <div class="offset-3 col-6">
      <div class="card">
        <div class="card-body">
          <div class="row text-center">
            <div class="card-title">Edit Wundershort</div>
          </div>
          <div class="row">
            <div class="col-2 d-flex flex-wrap align-items-center">
              <img src="{{ asset('images/mario-shrink.gif') }}" class="card-img-top d-flex flex-wrap align-items-center" style="height: 50%;" alt="mario-shrink">
            </div>
            <div class="col-10 text-center">
              <div class="card-body">
                @if( count( $errors ) )
                  <div class="alert alert-danger text-start offset-1 col-10">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                  </div>
                  <div class="offset-3"></div>
                @endif
                <form class="offset-1 col-10" method="POST" action="{{ route('update-wundershort', $wundershort->id ) }}">
                  @csrf
  
                  <!-- Title -->
                  <div class="form-group mt-4">
                    <input id="title" class="form-control block mt-1" type="text" name="title" placeholder="Wundershort title" value="{{ $wundershort->title }}" autofocus required/>
                  </div>

                  <!-- URL -->
                  <div class="form-group mt-4">
                    <input id="original_url" class="form-control block mt-1" type="text" name="original_url" placeholder="Original URL" value="{{ $wundershort->original_url }}" required/>
                  </div>
  
                  <div class="flex items-center justify-center mt-4 me-5 pe-5">
                      <button class="me-3 btn btn-warning" >Update</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>
@endsection
