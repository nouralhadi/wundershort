@extends('layouts.main')
@section('content')
  <div class="row">
    <div class="offset-2 col-8">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12 d-flex align-items-stretch">
          <div class="card" style="width: 18rem;">
            <img width="175" height="150" src="{{ asset('images/mario-shrink.gif') }}" class="card-img-top" alt="mario-shrink">
            <div class="card-body">
              <h5 class="card-title">Get a new Wundershort&trade;</h5>
              <p class="card-text">It starts with a button click! get yourself a new Wundershort account now and start shortning your links as easy as one button click!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 d-flex align-items-stretch">
          <div class="card" style="width: 18rem;">
            <img width="175" height="150" src="{{ asset('images/mario-walking.gif') }}" class="card-img-top" alt="mario-walking">
            <div class="card-body">
              <h5 class="card-title">Share your Wundershort&trade;</h5>
              <p class="card-text">Now that you've got yourself a Wundershort pass it between your friends to get easier access to any website on the WWW!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 d-flex align-items-stretch">
          <div class="card" style="width: 18rem;">
            <img width="175" height="150" src="{{ asset('images/mario-grow.gif') }}" class="card-img-top" alt="mario-walking">
            <div class="card-body">
              <h5 class="card-title">Wundershort&trade; to WWW</h5>
              <p class="card-text">Now comes our part! when someone uses one of your Wundershorts our servers will redirect them to the target location keeping some analytical info to help you get the most benefits of your Wundershorts.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12 text-center">So what are you waiting for? <a class="nostyle text-bold text-success text-light" href="{{ route('home') }}">Join us!</a></div>
      </div>
      
    </div>
  </div>
@endsection
