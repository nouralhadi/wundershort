<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/gridjs/dist/theme/mermaid.min.css" rel="stylesheet" />
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <style>
            html {
              margin: 0;
              padding: 0;
              box-sizing: border-box;
            }
            body {
                background-color: #EFF2F1;
                font-family: 'Nunito', sans-serif;
                width: 100%;
                margin: 0;
                padding: 0;
                box-sizing: border-box
            }
            .bg-success, .alert-success {
              background-color: #226F54!important;
            }

            .gridjs-table {
              width: 100%
            }

            .text-success {
              color: #226F54!important;
            }

            a.nostyle:link {
                text-decoration: inherit;
                color: inherit;
                cursor: pointer;
            }

            a.nostyle:visited {
                text-decoration: inherit;
                color: inherit;
                cursor: pointer;
            }

            html {
              position: relative;
              min-height: 100%;
            }
            body {
              margin-bottom: 60px; /* Margin bottom by footer height */
            }
            .footer {
              position: absolute;
              bottom: 0;
              width: 100%;
              height: 60px; /* Set the fixed height of the footer here */
              line-height: 60px; /* Vertically center the text there */
            }
        </style>
        @yield('styles')
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{route('home')}}">{{config('app.name')}}</a>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link {{ Route::currentRouteName() === "home" ? "active": "" }}" aria-current="page" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ Route::currentRouteName() === "new-wundershort" ? "active": "" }}" href="{{ route('new-wundershort') }}">New Short</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ Route::currentRouteName() === "statistics" || Route::currentRouteName() === "one-statistics" ? "active": "" }}" href="{{ route('statistics') }}">Statistics</a>
                </li>
              </ul>
              @if( Auth::user() )
                <ul class="navbar-nav px-4 border-t border-gray-200">
                  <li class="nav-item mx-4">
                    <div class="space-y-1">
                        <a class="nostyle text-white pointer" href="{{ route('home') }}">
                          Welcome {{ explode( " ", Auth::user()->name )[0] }}
                        </a>
                    </div>
                  </li>
                  <li class="nav-item">
                    <div class="space-y-1">
                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="nostyle text-white pointer" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
                                Log out
                            </a>
                        </form>
                    </div>
                  </li>
                </ul>
              @else
                <ul class="navbar-nav px-4 border-t border-gray-200">
                  <li class="nav-item mx-2">
                    <a class="nostyle text-white pointer" href="{{ route('login') }}"> Login </a>
                  </li>

                  <li class="nav-item mx-2">
                    <a class="nostyle text-white pointer" href="{{ route('register') }}"> Register </a>
                  </li>
                </ul>
              @endif
            </div>
          </div>
        </nav>
        <div class="container-fluid mt-5">
          @yield('content')
        </div>

        <footer class="footer text-center">
          <div class="container">
            <span class="text-muted">All rights reserverd &copy; <script> document.write(new Date().getFullYear()) </script> Wundershort&trade; by Nour Alhadi Mahmoud.</span>
          </div>
        </footer>
        
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>      
        <script src="https://cdn.jsdelivr.net/npm/gridjs/dist/gridjs.umd.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <!-- amCharts -->
        <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
        <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
        @yield('scripts')
    </body>
</html>